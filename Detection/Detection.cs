﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.VideoSurveillance;
using Emgu.CV.Features2D;
using Emgu.CV.Util;
using System.Drawing;

namespace Detection
{
    public class Detection
    {
        private static Capture _cameraCapture;
        // must be odd numbers or an exception is thrown
        private int _blurr = 5;
        private int _thresholdSmooth = 61;
        private double _uniquenessThreshold = 0.4;
        private int _precision = 4;

        private int _frameAvg = 8; // On how many frame the average is done
        private int _frameMinimum = 1; // What is the minimum number on this frames to consider the tag in

        public delegate void OnEventTrigger(int i);

        public OnEventTrigger OnActivated
        {
            get;
            set;
        }

        public OnEventTrigger OnDeactivated
        {
            get;
            set;
        }

        public Detection()
        {
            _cameraCapture = new Capture();
            LoadTag("tag1.png", 0);
            LoadTag("tag2.png", 1);
            LoadTag("tag3.png", 2);
            LoadTag("tag1.png", 3);
            Task.Factory.StartNew(ProcessFrame);
        }

        private Rectangle[] _rects = new Rectangle[4]
        {
            new Rectangle(375, 234, 60, 72),
            new Rectangle(166, 118, 166, 98),
            new Rectangle(41, 270, 101, 73),
            new Rectangle(0, 0, 400, 400)
        };
        private Image<Gray, Byte>[] _tags = new Image<Gray, Byte>[4];

        void LoadTag(string path, int idx)
        {
            _tags[idx] = new Image<Gray, byte>(path);
            Image<Gray, Byte> transformed = _tags[idx].Convert<Gray, byte>(); // Convert to black and white
            System.IntPtr srcPtr = transformed.Ptr;

            //Adaptive threshold
            CvInvoke.cvAdaptiveThreshold(srcPtr, srcPtr, 255,
                Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C,
                Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY, _thresholdSmooth, 0);

            transformed._SmoothGaussian(_blurr);
            _tags[idx] = transformed;
        }

        private bool FindMatch(Image<Gray, Byte> frame,
                               Image<Gray, byte> tag)
        {
            VectorOfKeyPoint modelKeyPoints;
            VectorOfKeyPoint observedKeyPoints;
            Matrix<int> indices;
            Matrix<byte> mask;

            int k = 2;
            SURFDetector surfCPU = new SURFDetector(500, false);

            //extract features from the object image
            modelKeyPoints = new VectorOfKeyPoint();
            Matrix<float> modelDescriptors = surfCPU.DetectAndCompute(tag, null, modelKeyPoints);

            // extract features from the observed image
            observedKeyPoints = new VectorOfKeyPoint();
            Matrix<float> observedDescriptors = surfCPU.DetectAndCompute(frame, null, observedKeyPoints);
            BruteForceMatcher<float> matcher = new BruteForceMatcher<float>(DistanceType.L2);
            matcher.Add(modelDescriptors);

            if (observedDescriptors == null)
                return (false);
            indices = new Matrix<int>(observedDescriptors.Rows, k);
            using (Matrix<float> dist = new Matrix<float>(observedDescriptors.Rows, k))
            {
                matcher.KnnMatch(observedDescriptors, indices, dist, k, null);
                mask = new Matrix<byte>(dist.Rows, 1);
                mask.SetValue(255);
                Features2DToolbox.VoteForUniqueness(dist, _uniquenessThreshold, mask);
            }

            int nonZeroCount = CvInvoke.cvCountNonZero(mask);
            if (nonZeroCount >= _precision)
            {
                nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, indices, mask, 1.5, 20);
                if (nonZeroCount >= _precision)
                    return (true);
            }
            return (false);
        }

        private int[] _curFrame = new int[4] { 0, 0, 0, 0 };
        private int[] _succeed = new int[4] { 0, 0, 0, 0 };
        private bool[] _isIn = new bool[4] { false, false, false, false };
        private bool[] _wasIn = new bool[4] { false, false, false, false };

        private void CheckTags(Image<Bgr, Byte> frame)
        {
            Image<Gray, Byte> transformed = frame.Convert<Gray, byte>(); // Convert to black and white
            System.IntPtr srcPtr = transformed.Ptr;

            //Adaptive threshold
            CvInvoke.cvAdaptiveThreshold(srcPtr, srcPtr, 255,
                Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C,
                Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY, _thresholdSmooth, 0);

            transformed._SmoothGaussian(_blurr);
            for (int i = 0; i < _isIn.Length; ++i)
            {
                transformed.ROI = _rects[i];
                bool res = FindMatch(transformed, _tags[i]);

                if (res)
                    ++_succeed[i];
                ++_curFrame[i];
                if (_curFrame[i] == _frameAvg)
                {
                    _curFrame[i] = 0;
                    if (_succeed[i] > _frameMinimum)
                        _isIn[i] = true;
                    else
                        _isIn[i] = false;
                    _succeed[i] = 0;
                }
                if (_isIn[i] && !_wasIn[i])
                {
                    if (OnActivated != null)
                        OnActivated(i);
                }
                else if (!_isIn[i] && _wasIn[i])
                {
                    if (OnDeactivated != null)
                        OnDeactivated(i);
                }
            }
        }

        void ProcessFrame()
        {
            while(true)
            {
                Image<Bgr, Byte> frame = _cameraCapture.QueryFrame();

                try
                {
                    CheckTags(frame);
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine(ex.Message);
                }
                for (int i = 0; i < _isIn.Length; ++i)
                {
                    _wasIn[i] = _isIn[i];
                }
            }
        }
    }
}
