﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QuickGraph;
using GraphSharp;
using GraphSharp.Controls;
using System.ComponentModel;

namespace iCube
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MuseomixViewModel _viewModel;
        private Detection.Detection _detect;

        public MainWindow()
        {
            _viewModel = new MuseomixViewModel();
            this.DataContext = _viewModel;
            InitializeComponent();
            try
            {
                StartDetection();
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            
        }

        private void StartDetection()
        {
            _detect = new Detection.Detection();
            _detect.OnActivated += new Detection.Detection.OnEventTrigger((i) =>
            {
                switch (i)
                {
                    case 0:
                        _viewModel.AddCriteria(CriteriaName.Couleur);
                        break;
                    case 1:
                        _viewModel.AddCriteria(CriteriaName.Materiau);
                        break;
                    case 2:
                        _viewModel.AddCriteria(CriteriaName.Forme);
                        break;
                    case 3:
                        _viewModel.AddCriteria(CriteriaName.Date);
                        break;
                }
            });
            _detect.OnDeactivated += new Detection.Detection.OnEventTrigger((i) =>
            {
                switch (i)
                {
                    case 0:
                        _viewModel.RemoveCriteria(CriteriaName.Couleur);
                        break;
                    case 1:
                        _viewModel.RemoveCriteria(CriteriaName.Materiau);
                        break;
                    case 2:
                        _viewModel.RemoveCriteria(CriteriaName.Forme);
                        break;
                    case 3:
                        _viewModel.RemoveCriteria(CriteriaName.Date);
                        break;
                }
            });
        }

        private void ToggleColor_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ToggleCriteria(CriteriaName.Couleur);
        }

        private void ToggleMaterial_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ToggleCriteria(CriteriaName.Materiau);
        }

        private void ToggleForm_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ToggleCriteria(CriteriaName.Forme);
        }

        private void ToggleDate_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ToggleCriteria(CriteriaName.Date);
        }
    }
}
