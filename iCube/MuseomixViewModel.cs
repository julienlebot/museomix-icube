﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Media;
using QuickGraph;
using GraphSharp.Controls;
using GraphSharp;
using GraphSharp.Algorithms.OverlapRemoval;
using System.Windows.Media.Imaging;
using System.Windows.Data;

namespace iCube
{
    public enum CriteriaName
    {
        Couleur,
        Date,
        Artiste,
        Forme,
        Materiau
    }

    public class MuseomixViewModel : INotifyPropertyChanged
    {
        private MuseomixGraph _graph = new MuseomixGraph(true);
        private Repository _repository = new Repository();
        private List<CriteriaName> _criterias = new List<CriteriaName>();

        public OverlapRemovalParameters OverlapRemovalParams
        {
            get
            {
                return (new OverlapRemovalParameters { HorizontalGap = 10, VerticalGap = 10 });
            }
        }

        public MuseomixGraph Graph
        {
            get
            {
                return _graph;
            }
            set
            {
                _graph = value;
            }
        }

        public void ToggleCriteria(CriteriaName name)
        {
            if (!_criterias.Contains(name))
            {
                AddCriteria(name);
            }
            else
            {
                RemoveCriteria(name);
            }
            UpdateCriterias();
        }

        public void AddCriteria(CriteriaName name)
        {
            if (!_criterias.Contains(name))
            {
                _criterias.Add(name);
                UpdateCriterias();
            }
        }

        public void RemoveCriteria(CriteriaName name)
        {
            _criterias.Remove(name);
            UpdateCriterias();
        }

        private void UpdateCriterias()
        {
            Graph.Clear();
            Graph = new MuseomixGraph();
            foreach (var m in _repository.Models)
            {
                Graph.AddVertex(m);
            }
            foreach (var c in _criterias)
            {
                switch (c)
                {
                    case CriteriaName.Couleur:
                        {
                            foreach (var m1 in _repository.Models)
                            {
                                var collection = from m in _repository.Models where m.Colour == m1.Colour select m;
                                foreach (var m2 in collection)
                                {
                                    MuseomixEdge newEdge = new MuseomixEdge(Colors.Pink, m1, m2);
                                    Graph.AddEdge(newEdge);
                                }
                            }
                        }
                        break;
                    case CriteriaName.Forme:
                        {
                            foreach (var m1 in _repository.Models)
                            {
                                var collection = from m in _repository.Models where m.Form == m1.Form select m;
                                foreach (var m2 in collection)
                                {
                                    MuseomixEdge newEdge = new MuseomixEdge(Colors.Orchid, m1, m2);
                                    Graph.AddEdge(newEdge);
                                }
                            }
                        }
                        break;
                    case CriteriaName.Materiau:
                        {
                            foreach (var m1 in _repository.Models)
                            {
                                var collection = from m in _repository.Models where m.Material == m1.Material select m;
                                foreach (var m2 in collection)
                                {
                                    MuseomixEdge newEdge = new MuseomixEdge(Colors.Olive, m1, m2);
                                    Graph.AddEdge(newEdge);
                                }
                            }
                        }
                        break;
                    case CriteriaName.Date:
                        {
                            foreach (var m1 in _repository.Models)
                            {
                                var collection = from m in _repository.Models where m.Date == m1.Date select m;
                                foreach (var m2 in collection)
                                {
                                    MuseomixEdge newEdge = new MuseomixEdge(Colors.Brown, m1, m2);
                                    Graph.AddEdge(newEdge);
                                }
                            }
                        }
                        break;
                }
            }
            NotifyPropertyChanged("Graph");
        }

        public MuseomixViewModel()
        {
            foreach (var m in _repository.Models)
            {
                Graph.AddVertex(m);
            }
        }

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion
    }

    public class MuseomixModel
    {
        public String Name
        {
            get;
            set;
        }

        public String Date
        {
            get;
            set;
        }

        public Color Colour
        {
            get;
            set;
        }

        public String Material
        {
            get;
            set;
        }

        public String Form
        {
            get;
            set;
        }

        public String Image
        {
            get;
            set;
        }

        public ImageSource ImageSource
        {
            get
            {
                var uriSource = new Uri("/iCube;component/picto/" + Image, UriKind.Relative);
                return new BitmapImage(uriSource);
            }
        }
    }

    public class Criteria : IEquatable<Criteria>
    {
        public CriteriaName Name
        {
            get;
            set;
        }

        public Color Colour
        {
            get;
            set;
        }

        public Criteria()
        {
        }

        public Boolean Equals(Criteria other)
        {
            return (other.Name == Name);
        }
    }

    public class Repository
    {
        public IEnumerable<MuseomixModel> Models
        {
            get;
            protected set;
        }

        public Repository()
        {
            var rand = new Random();
            var mdls = new List<MuseomixModel>()
            {
                new MuseomixModel
                {
                    Name = "ChaiseLongue",
                    Colour = Colors.Black,
                    Date = "1980-1985",
                    Material = "Cuir",
                    Form = "Rond",
                    Image = "PICTO_CHAISESZEKELI.png"
                },
                new MuseomixModel
                {
                    Name = "Tabouret",
                    Colour = Colors.Black,
                    Date = "1985-1990",
                    Material = "Bois",
                    Form = "Triangle",
                    Image = "PICTO_CHAISETRIANGLE.png"
                },
                new MuseomixModel
                {
                    Name = "Lustre",
                    Colour = Colors.White,
                    Date = "2000-2013",
                    Material = "Verre",
                    Form = "Triangle",
                    Image = "PICTO_LUSTRE.png"
                },
                new MuseomixModel
                {
                    Name = "ChaiseImperiale",
                    Colour = Colors.Orange,
                    Date = "1980-1985",
                    Material = "Bois",
                    Form = "Rond",
                    Image = "PICTO_PAILLE.png"
                },
                new MuseomixModel
                {
                    Name = "BureauDubuisson",
                    Colour = Colors.Beige,
                    Date = "1990-1995",
                    Material = "Bois",
                    Form = "Rond",
                    Image = "Picto-table-dubuisson.png"
                },
                new MuseomixModel
                {
                    Name = "TableZekeli",
                    Colour = Colors.Black,
                    Date = "1980-1985",
                    Material = "Acier",
                    Form = "Triangle",
                    Image = "Picto-table-szekeli.png"
                },
                new MuseomixModel
                {
                    Name = "LuminaireDubuisson",
                    Colour = Colors.Black,
                    Date = "1990-1995",
                    Material = "Plastique",
                    Form = "Triangle",
                    Image = "Picto-lampadaire-dubuisson.png"
                },
                new MuseomixModel
                {
                    Name = "FauteuilGaniere",
                    Colour = Colors.Violet,
                    Date = "1990-1995",
                    Material = "Velours",
                    Form = "Carre",
                    Image = "Picto-fauteuil-gagniere.png"
                },
                new MuseomixModel
                {
                    Name = "EtagereBauchet",
                    Colour = Colors.Gray,
                    Date = "2000-2013",
                    Material = "Plastique",
                    Form = "Carre",
                    Image = "Picto-etagere.png"
                },
                new MuseomixModel
                {
                    Name = "TableBasseBonetille",
                    Colour = Colors.Blue,
                    Date = "1980-1985",
                    Material = "Pierre",
                    Form = "Triangle",
                    Image = "Picto_table_cailloux.png"
                }
            };
            Models = mdls;
        }
    }

    public class MuseomixEdge : TypedEdge<MuseomixModel>, INotifyPropertyChanged
    {
        public Color Colour
        {
            get;
            set;
        }

        public MuseomixEdge(Color color, MuseomixModel source, MuseomixModel target)
            : base(source, target, EdgeTypes.General)
        {
            Colour = color;
        }

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion
    }

    public class EdgeColorConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new SolidColorBrush((Color)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MuseomixGraph : BidirectionalGraph<MuseomixModel, MuseomixEdge>
    {
        public MuseomixGraph()
        {
        }

        public MuseomixGraph(bool allowParallelEdges)
            : base(allowParallelEdges)
        {
        }

        public MuseomixGraph(bool allowParallelEdges, int vertexCapacity)
            : base(allowParallelEdges, vertexCapacity)
        {
        }
    }

    public class MuseomixLayout : GraphLayout<MuseomixModel, MuseomixEdge, MuseomixGraph> { }
}
